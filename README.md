### Quantum Wordle Game:

The Quantum Wordle game is a word-guessing game where players attempt to guess a secret 5-letter word chosen randomly from a list of English words. The game utilizes quantum computing concepts to determine the number of correct letters guessed by the player. Players make guesses by inputting 5-letter words, and the game provides feedback on the number of correctly guessed letters.

# Demo

![demo](https://gitlab.com/graylan01/quantum-word-game/-/raw/main/demo.png) 
### Installation:

#### 1. Install Python:

Ensure that Python 3.x is installed on your system. You can download Python from the official website: [python.org](https://www.python.org/downloads/)

#### 2. Clone the Repository:

Clone the Quantum Wordle game repository from GitHub:
```
git clone https://gitlab.com/graylan01/quantum-word-game
```

#### 3. Install Dependencies:

Navigate to the project directory and install the required dependencies using pip:
```
cd quantum-word-game
pip install -r requirements.txt
```

### Deployment:

#### Windows:

1. Open Command Prompt or PowerShell.
2. Navigate to the project directory.
3. Run the game:
   ```
   python main.py
   ```

#### macOS / Linux:

1. Open Terminal.
2. Navigate to the project directory.
3. Run the game:
   ```
   python main.py
   ```

### Building Executable (Windows):

To create an executable file (.exe) for the Quantum Wordle game on Windows, you can use PyInstaller.

#### 1. Install PyInstaller:

Install PyInstaller using pip:
```
pip install pyinstaller
```

#### 2. Build the Executable:

Navigate to the project directory and run PyInstaller:
```
pyinstaller --onefile main.py
```

This will generate a standalone executable file in the `dist` directory.

### Playing the Game:

Once the game is launched, follow the on-screen instructions to play the Quantum Wordle game. Input your guesses using the provided input fields and click the "Submit" button to check your guess. The game will provide feedback on the number of correct letters guessed. Try to guess the word within 6 attempts to win the game.

### Summary:

- **Installation**: Install Python, clone the repository, and install dependencies.
- **Deployment**: Run the game using Python on Windows, macOS, or Linux.
- **Building Executable**: Use PyInstaller to create a standalone executable file for Windows.
- **Playing the Game**: Input guesses and click "Submit" to check correctness.
 