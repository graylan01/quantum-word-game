import random
import nltk
from nltk.corpus import words
import pennylane as qml
import numpy as np
import logging
from kivymd.app import MDApp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRaisedButton
from kivymd.uix.textfield import MDTextField
from kivymd.uix.dialog import MDDialog
from kivymd.uix.label import MDLabel
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen, ScreenManager

# Download the words corpus
nltk.download('words')
word_list = [word.upper() for word in words.words() if len(word) == 5]

def generate_secret_word():
    return random.choice(word_list)

secret_word = generate_secret_word()

dev = qml.device('default.qubit', wires=5)

@qml.qnode(dev)
def quantum_word_check(input_word, secret_word):
    input_word = [ord(c) - 65 for c in input_word]
    secret_word = [ord(c) - 65 for c in secret_word]

    for i in range(5):
        qml.Hadamard(wires=i)

    for i in range(5):
        qml.RY(input_word[i] * np.pi / 26, wires=i)
        qml.RY(secret_word[i] * np.pi / 26, wires=i)

    for i in range(5):
        qml.CNOT(wires=[i, (i + 1) % 5])

    return qml.probs(wires=range(5))

def evaluate_guess(guess):
    probabilities = quantum_word_check(guess, secret_word)
    max_prob = np.argmax(probabilities)
    match_chars = bin(max_prob).count('1')
    return match_chars

KV = '''
ScreenManager:
    id: screen_manager

    MainScreen:
        name: "main_screen"

<MainScreen>:
    BoxLayout:
        orientation: 'vertical'
        padding: [20, 20, 20, 20]
        spacing: 10
        canvas.before:
            Color:
                rgba: (0.2, 0.2, 0.2, 1)
            Rectangle:
                size: self.size
                pos: self.pos

        MDLabel:
            text: "Quantum Wordle Game"
            halign: 'center'
            font_style: 'H4'
            theme_text_color: "Custom"
            text_color: (1, 0.8, 0.8, 1)
            size_hint_y: None
            height: self.texture_size[1]

        MDLabel:
            text: "Guess the 5-letter word:"
            halign: 'center'
            font_style: 'Subtitle1'
            theme_text_color: "Custom"
            text_color: (0.8, 1, 0.8, 1)
            size_hint_y: None
            height: self.texture_size[1]

        MDBoxLayout:
            id: input_box
            orientation: 'horizontal'
            spacing: 10
            size_hint_y: None
            height: self.minimum_height

        MDRaisedButton:
            text: "Submit"
            size_hint_y: None
            height: "50dp"
            md_bg_color: (0.6, 0.4, 1, 1)
            on_release: app.check_guess()

        BoxLayout:
            id: result_box
            orientation: 'vertical'
            size_hint_y: None
            height: self.minimum_height
            spacing: 10

        Widget:
            size_hint_y: None
            height: 20
'''

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

class MainScreen(Screen):
    pass

class WordleApp(MDApp):
    def build(self):
        try:
            logging.info("App build started")
            self.secret_word = generate_secret_word()
            logging.debug(f"Generated secret word: {self.secret_word}")
            self.guess_attempts = 0
            self.dialog = None
            return Builder.load_string(KV)
        except Exception as e:
            logging.error(f"Error in build method: {e}")
            logging.error(f"Attempting to show error dialog: {self.show_dialog}")
            self.show_dialog("Error", "An error occurred during the app build.")

    def on_start(self):
        try:
            logging.info("App started")
            self.add_input_fields()
        except Exception as e:
            logging.error(f"Error in on_start method: {e}")
            self.show_dialog("Error", "An error occurred while starting the app.")


    def add_input_fields(self):
        try:
            input_box = self.root.get_screen('main_screen').ids.input_box
            input_box.clear_widgets()
            self.input_fields = []
            for _ in range(5):
                input_field = MDTextField(
                    size_hint=(None, None),
                    width=50,
                    height=50,
                    halign='center',
                    max_text_length=1,
                    input_filter=self.filter_input,
                    background_color=(1, 0.8, 0.8, 1),
                    foreground_color=(0, 0, 0, 1)
                )
                self.input_fields.append(input_field)
                input_box.add_widget(input_field)
            logging.info("Input fields added")
        except Exception as e:
            logging.error(f"Error in add_input_fields method: {e}")
            self.show_dialog("Error", "An error occurred while adding input fields.")

    def filter_input(self, text, from_undo):
        return text.upper() if len(text) < 2 else text[-1].upper()

    def show_dialog(self, title, text):
        try:
            if self.dialog is not None:
                self.dialog.dismiss()

            self.dialog = MDDialog(
                title=title,
                text=text,
                buttons=[MDRaisedButton(text="OK", on_release=lambda x: self.dialog.dismiss())],
                radius=[20, 20, 20, 20]
            )
            self.dialog.open()
            logging.info(f"Dialog shown: {title} - {text}")
        except Exception as e:
            logging.error(f"Error in show_dialog method: {e}")

    def check_guess(self):
        try:
            guess = ''.join([field.text.upper() for field in self.input_fields])
            logging.debug(f"User guess: {guess}")
            if len(guess) != 5 or any(c not in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' for c in guess):
                self.show_dialog("Invalid Guess", "Please enter a valid 5-letter word.")
                logging.warning("Invalid guess entered")
                return

            match_count = evaluate_guess(guess)
            logging.debug(f"Match count: {match_count}")
            self.display_result(guess, match_count)
            self.guess_attempts += 1
            logging.info(f"Guess attempts: {self.guess_attempts}")

            if match_count == 5:
                self.show_dialog("Congratulations!", f"You've guessed the word '{self.secret_word}' in {self.guess_attempts} attempts!")
                logging.info("Word guessed correctly")
                self.reset_game()
            elif self.guess_attempts >= 6:
                self.show_dialog("Game Over", f"You've used all attempts. The word was '{self.secret_word}'.")
                logging.info("Game over: attempts exhausted")
                self.reset_game()
        except Exception as e:
            logging.error(f"Error in check_guess method: {e}")
            self.show_dialog("Error", "An error occurred while checking the guess.")

    def display_result(self, guess, match_count):
        try:
            result_box = self.root.get_screen('main_screen').ids.result_box
            result_text = f"{guess}: {match_count} correct letters"
            result_label = MDLabel(
                text=result_text,
                size_hint_y=None,
                height=40,
                theme_text_color="Custom",
                text_color=(1, 1, 0, 1)
            )
            result_box.add_widget(result_label)
            logging.info(f"Displayed result: {result_text}")
        except Exception as e:
            logging.error(f"Error in display_result method: {e}")
            self.show_dialog("Error", "An error occurred while displaying the result.")


    def show_dialog(self, title, text):
        try:
            if self.dialog is not None:
                self.dialog.dismiss()

            self.dialog = MDDialog(
                title=title,
                text=text,
                buttons=[MDRaisedButton(text="OK", on_release=lambda x: self.dialog.dismiss())],
                radius=[20, 20, 20, 20]
            )
            self.dialog.open()
            logging.info(f"Dialog shown: {title} - {text}")
        except Exception as e:
            logging.error(f"Error in show_dialog method: {e}")

    def reset_game(self):
        try:
            self.secret_word = generate_secret_word()
            self.guess_attempts = 0
            result_box = self.root.get_screen('main_screen').ids.result_box
            result_box.clear_widgets()
            self.add_input_fields()
            logging.info("Game reset")
        except Exception as e:
            logging.error(f"Error in reset_game method: {e}")
            self.show_dialog("Error", "An error occurred while resetting the game.")

if __name__ == "__main__":
    try:
        app = WordleApp()  # Create an instance of WordleApp
        app.run()          # Run the application
    except Exception as e:
        logging.critical(f"Critical error in main execution: {e}")